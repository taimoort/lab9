#include <stdio.h>
#include <stdlib.h>

typedef void(*multFunc)(struct MyMat*, struct MyMat*, struct MyMat*);
typedef void(*addFunc)(struct MyMat*, struct MyMat*, struct MyMat*);
typedef void(*vmethod)(void* matrix);
vmethod *vtable_mat_class;
vmethod *vtable_vec_class;

typedef struct MyMat 
{
	int rows;
	int col;
	int *structArr;
	multFunc mult;
	addFunc add;
	vmethod *vtable_ptr;

} MyMat;

typedef struct MyVec
{	
	int min;
	int max;
	MyMat mat;
	vmethod *vtableptr;
} MyVec;


void Vec_Norm(MyMat* this)
{
	
	struct MyVec* v = (MyVec*)this;
	
	int j = 0, sum = 0;
	
		for (j = 0; j < v->mat.col; j++)
		{

			sum += v->mat.structArr[j];
			
		}
		printf("\nL1 norm (Sum of all elements of the vector): %d\n\n", sum);
}

void Mat_Norm(MyMat* this)
{
	int i = 0, j = 0;
	printf("\nSum of columns of matrix is:\n");
	for (i = 0; i < this->rows; i++)
	{
		this->structArr[i] = 0;
		for (j = 0; j < this->col ; j++)
		{
			
			this->structArr[i] += this->structArr[i*this->col + j];
			
		}
		printf("%d\n", this->structArr[i]);
	}
	int max;
	max = this->structArr[0];
	for (i = 1; i < this->rows; i++)
	{
		if (this->structArr>max)
		{
			max = this->structArr[i];

		}
	}
	printf("\nL1 norm (max of the sums above): %d\n\n", max);
}

void Vec_Add(MyVec *v1, MyVec *v2, MyVec *vR)
{
	int i = 0, j = 0;
	for (i = 0; i < 1; i++)
	{

		for (j = 0; j < v1->mat.col; j++)
		{
			vR->mat.structArr[i*vR->mat.col + j] = v1->mat.structArr[i*v1->mat.col + j] + v2->mat.structArr[i*v2->mat.col + j];
			printf("%d ", vR->mat.structArr[i*vR->mat.col + j]);
		}
		printf("\n");
	}
}

void Mat_Add(MyMat *m1, MyMat *m2, MyMat *mR)
{
	int i = 0, j = 0;
	for (i = 0; i < m1->rows; i++)
	{
		for (j = 0; j < m1->col; j++)
		{
			mR->structArr[i*mR->col + j] = m1->structArr[i*m1->col+j] +  m2->structArr[i*m2->col + j];
			printf("%d ", mR->structArr[i*mR->col + j]);
		}
		printf("\n");
	}
}

void Mat_Mult(MyMat *m1, MyMat *m2, MyMat *mR)
{
	int i = 0, j = 0, k = 0;
	for (i = 0; i < m1->rows; i++)
	{
		for (j = 0; j < m2->col; j++)
		{
			int sum = 0;
			for (k = 0; k < m1->col; k++)
			{
				sum += m1->structArr[i*m1->col + k] * m2->structArr[k*m2->col + j];

			}
			mR->structArr[i*mR->col + j] = sum;
			printf("%d ", mR->structArr[i*mR->col + j]);
		}
		printf(" \n");
	}
}

void init_MyMat(MyMat *x, int r, int c)
{
	x->rows = r;
	x->col = c;
	x->add = Mat_Add;
	x->mult = Mat_Mult;
	x->structArr = malloc(sizeof(int)*r*c);
	int i = 0, j = 0;
	for (i = 0; i < r; i++)
	{
		for (j = 0; j < c; j++)
		{
			x->structArr[i*c + j] = j + i;
		}
	}
}

void init_MyVec(MyVec *x1, int c1)
{
	x1->mat.col = c1;
	x1->mat.rows = 1;
	x1->mat.add=Vec_Add;
	x1->min=0;
	x1->max=0;
	x1->mat.structArr = malloc(sizeof(int)*x1->mat.col);
	int i = 0, j = 0;
	for (i = 0; i < 1; i++)
	{
		for (j = 0; j < c1; j++)
		{
			x1->mat.structArr[i*c1 + j] = j + i;
		}
	}
}

void main()
{
	vtable_vec_class = (vmethod*)malloc(sizeof(vmethod));
	vtable_mat_class = (vmethod*)malloc(sizeof(vmethod));
	vtable_vec_class[0]=Vec_Norm;
	vtable_mat_class[0]=Mat_Norm;
	
	int i=0;
	int n=0;

	printf("Press 1 for Matrix\nPress 2 for Vector")
	scanf("%d",&i);
	if (i==1)
	{
		MyMat m1;
		MyMat mRes;
		init_MyMat(&m1, 4, 4);
		init_MyMat(&mRes, 4, 4);
		int i = 0, j = 0;
		printf("The Generated Matrix is: \n");
		for (i = 0; i <m1.rows; i++)
		{
			for (j = 0; j < m1.col; j++)
			{
				printf("%d ", m1.structArr[i * m1.col + j]);
			}
			printf("\n");
		}
	scanf("%d",&n);
	printf("\nPress 1 for add of matrix with itself.\nPress 2 for multiplication of matrix with itself.\nPress 3 for Norm.");
	printf("\n");
	
		if (n==1)
		{
			mRes.add(&m1, &m1, &mRes);
			printf("\n");
		}
		else if (n==2)
		{
			mRes.mult(&m1, &m1, &mRes);
			printf("\n");
		}
		else if (n==3)
		{
			mRes.vtable_ptr=vtable_mat_class;
			mRes.vtable_ptr[0](&m1);
		}
		else
		{
			printf("Invalid input.\n");
		}
		printf("\n");
	}

	else if (i==2)
	{
		MyVec v1;
		MyVec vRes;
		init_MyVec(&v1, 6);
		init_MyVec(&vRes, 6);
		printf("The Generated Vector is: \n");
		for (i = 0; i <1; i++)
		{
			for (j = 0; j < v1.mat.col; j++)
			{
				printf("%d ", v1.mat.structArr[i * m1.col + j]);
			}
			printf("\n");
		}
		printf("Press 1 for add of vector with itself.\nPress 2 for Norm.\n");
		scanf("%d",&n);
		if (a==1)
		{
			vRes.mat.add(&v1, &v1, &vRes);
			printf("\n");
		}
		else if (a==2)
		{
			vRes.vtableptr = vtable_vec_class;
			vRes.vtableptr[0](&v1);
		}
		else
		{
			printf("Invalid input.\n");
		}
		printf("\n");
	}
	
	_getch();
}
Lab9
